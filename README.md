# About

The intent of this project is to provide a web calculator which helps players calculate how to reach the penetration and critical damage caps in ESO.

# Ideas

  - Import/Export definitions so that if the calculator becomes outdated, it can be "updated" by the user
  - Import/Export the group penetration options so that raid teams can share their base-line penetration
  - Import/Export the personal penetration options so that people can give build advice
  - Work out URL encoding so that configuration can be shared without importing or exporting
  - Light/dark mode toggle via cookie or URL flag
  - Ability to save a configuration to a cookie for easy self-recall
  - Ability to name and save multiple configurations to a cookie for multiple raid teams
  - Include ESO patch and calculator version numbers in the exports
